FROM registry.gitlab.com/creditok/docker-nginx-php-laravel/master:latest

COPY ./ /var/www/html/
RUN chown -R www-data /var/www/html/storage/